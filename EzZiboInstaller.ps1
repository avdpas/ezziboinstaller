$RELEASE_NOTES  = @"

EzZibo Installer by Alex_NL74

v1.2.3
 - Minor fix in full installation logic where the zibo seach function looked in the xplane root instead of \Aircraft

v1.2.2
- minor fix in the update section to make sure temp folder is not there

v1.2.1
- Minor fix in .acf file search

v1.2.0
-Fixed new versioning in version.txt.
-fixed new version check in .acf file.

v1.1.1
- Fixed update file size for 4.00rc3.8

v1.1.0
- Changed code for zibo folder detection to not mess up with the default 737 or the upcomming LU737NG

v1.0.1
-Put a heavy search query for finding the Zibo into a variable array so i do not have to execute it 6 times and waste time.

v1.0
-Formatting of in app release notes changed.
-Added a trap to catch if more then 1 zibo is installed.
-Changed the way a zibo folder is detected to prevent not finding all if there are multiple
-Added checking for X-Plane mismatch in full and updates

v0.9d
-Fixed bug: update to same verion is possible.
-Added release notes to help menu
-Creation of zibo folder in aircraft is now with a capital Z
-Added conformation to succesful install
-Added conformation to succesful update
-Reverted release info, newest version is now on top

v0.9c
-Included a check if xPlane is running
-Small rewrite of zibofolder detect code
 to enhance performance a lot

 v0.9b
-Many bugfixes and added checks...
-This is a point of something that should work

v0.9a
-Initial Script

USE AT OWN RISK!!!! I CAN NOT BE HELD RESPONSIBLE!!

"@

$ZIBO_UPD_FILE  = "UNSET!"
$ZIBO_FULL_FILE = "UNSET!"
$XPLANE_PATH    = "UNSET!"
$XP_VERSION     = "UNSET!"
$XP_FULL_VERSION= "UNSET!"
$ZIBO_PATH      = "SELECT X-PLANE PATH FIRST!"
$ZIBO_VERSION   = "SELECT X-PLANE PATH FIRST!"
$EzZiboVERSION  = "v1.2.3"

###########################
## Creating the Menu
function Menu {

    Add-Type -assembly System.Windows.Forms
    [System.Windows.Forms.Application]::EnableVisualStyles()

    $main_form                 = New-Object System.Windows.Forms.Form
    $main_form.Text            = "Ez Zibo Installer ($EzZiboVERSION)... By Alex_NL74"
    $main_form.Width           = 900
    $main_form.Height          = 600
    $main_form.AutoSize        = $true
    $main_form.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog
    $main_form.startposition   = "centerscreen"

    ##Adding an icon
    $iconBase64     = 'AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAgBAAAAAAAAAAAAAAAAAAAAAAAAD/bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//////////////////////////////////////////////////bST//20k////////bST//20k////////bST//////////////////20k//9tJP//bST//20k//9tJP//bST//////////////////20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST///////9tJP//bST/////////////bST//20k//9tJP///////20k//9tJP//bST//20k////////bST//20k//9tJP///////20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP///////20k//9tJP///////20k//9tJP//bST//20k//9tJP///////20k//9tJP///////20k//9tJP//bST//20k//9tJP///////20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST///////9tJP//bST///////9tJP//bST//20k//9tJP//bST///////9tJP//bST///////9tJP//bST//20k//9tJP//bST///////9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP///////20k//9tJP///////20k//9tJP//bST//20k//9tJP///////20k//9tJP///////20k//9tJP//bST//20k//9tJP///////20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST///////9tJP//bST/////////////bST//20k//9tJP///////20k//9tJP//bST//20k////////bST//20k//9tJP///////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP///////20k//9tJP///////20k//////////////////9tJP//bST//20k//9tJP//bST//20k//////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k////////bST//20k//9tJP//bST///////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//////////////////////////////////////////////////9tJP///////20k//9tJP///////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP///////////////////////////////////////////////////////////////////////20k/////////////////////////////////////////////////////////////////////////////20k//9tJP//bST//20k////////////////////////////////////////////////////////////////////////bST/////////////////////////////////////////////////////////////////////////////bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//////////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k/////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//////////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//////////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k/////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/////////////////////////////////////////////////////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP////////////////////////////////////////////////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k/////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k/////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//////////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//////////////////20k//9tJP//bST//20k//9tJP//bST//20k/////////////20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST/////////////bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//////////////////bST//20k//9tJP//bST//20k//9tJP///////////////////////////////////////////////////////////////////////20k//9tJP//////////////////////////////////////////////////////////////////bST//20k//9tJP//bST//20k////////////////////////////////////////////////////////////////////////bST//20k//////////////////////////////////////////////////////////////////9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST//20k//9tJP//bST/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
    $iconBytes      = [Convert]::FromBase64String($iconBase64)
    $stream         = [System.IO.Memorystream]::new($iconBytes, 0, $iconBytes.Length)
    $main_Form.Icon = [System.Drawing.Icon]::FromHandle(([System.Drawing.Bitmap]::new($stream).GetHIcon()))

    ##adding a menu strip
    $menuMain         = New-Object System.Windows.Forms.MenuStrip
    $menuFile         = New-Object System.Windows.Forms.ToolStripMenuItem
    $menuExit         = New-Object System.Windows.Forms.ToolStripMenuItem
    $menuHelp         = New-Object System.Windows.Forms.ToolStripMenuItem
    $menuAbout        = New-Object System.Windows.Forms.ToolStripMenuItem
    $menuManual       = New-Object System.Windows.Forms.ToolStripMenuItem
    $menuReleaseNotes  = New-Object System.Windows.Forms.ToolStripMenuItem

    $main_form.MainMenuStrip = $menuMain
    $main_form.Controls.Add($menuMain)
    [void]$main_Form.Controls.Add($mainToolStrip)

    ########################
    #Show the menu toolbar
    $main_form.Controls.Add($menuMain)

    #Menu File
    $menuFile.text = "File"
    [void]$menuMain.Items.Add($menuFile)
    # Menu: File -> Exit
    $menuExit.Text = "Exit"
    $menuExit.Add_Click({$main_Form.Close()})
    [void]$menuFile.DropDownItems.Add($menuExit)

    #Menu Help
    $menuHelp.text = "Help"
    [void]$menuMain.Items.Add($menuHelp)
    #Menu Help -> About
    $menuAbout.text = "About"
    $menuAbout.Add_Click({ShowAbout})
    [void]$menuHelp.DropDownItems.Add($menuAbout)
    #Menu Help -> Manual
    $menuManual.text = "Manual"
    $menuManual.Add_Click({ShowManual})
    [void]$menuHelp.DropDownItems.Add($menuManual)
    #Menu Help -> ReleaseNotes
    $menuReleaseNotes.text = "Release Notes"
    $menuReleaseNotes.Add_Click({ShowReleaseNotes})
    [void]$menuHelp.DropDownItems.Add($menuReleaseNotes)


    ##########################
    ## Items in main menu.
    $main_form.controls.add($menuMain)

    ## Full zip file
    $fullziptxtboxlbl = New-Object System.Windows.Forms.label
    $fullziptxtboxlbl.location = New-Object System.Drawing.Point(180,50)
    $fullziptxtboxlbl.AutoSize = $true
    $fullziptxtboxlbl.Text = "Current selected full Zibo zip file"
    $main_form.controls.add($fullziptxtboxlbl)

    $setfullzipbtn               = New-Object Windows.Forms.Button
    $setfullzipbtn.text          = "Set Zibo full zip file"
    $setfullzipbtn.width         = 150
    $setfullzipbtn.height        = 40
    $setfullzipbtn.location      = New-Object System.Drawing.Point(20,50)
    $setfullzipbtn.Add_Click({SetZiboFullzip})
    $main_form.controls.add($setfullzipbtn)

    $fullziptxtbox           = New-Object system.Windows.Forms.TextBox
    $fullziptxtbox.multiline = $false
    $fullziptxtbox.size      = New-Object System.Drawing.Size(600,200)
    $fullziptxtbox.location  = New-Object System.Drawing.Point(180,70)
    $fullziptxtbox.Font      = 'Microsoft Sans Serif,12'
    $fullziptxtbox.Enabled   = $false
    $fullziptxtbox.Text      = $ZIBO_FULL_FILE
    $main_form.controls.add($fullziptxtbox)

    ## Update zip file
    $updziptxtboxlbl = New-Object System.Windows.Forms.label
    $updziptxtboxlbl.location = New-Object System.Drawing.Point(180,120)
    $updziptxtboxlbl.AutoSize = $true
    $updziptxtboxlbl.Text = "Current selected Zibo update zip file"
    $main_form.controls.add($updziptxtboxlbl)

    $setupdzipbtn               = New-Object Windows.Forms.Button
    $setupdzipbtn.text          = "Set Zibo update zip file"
    $setupdzipbtn.width         = 150
    $setupdzipbtn.height        = 40
    $setupdzipbtn.location      = New-Object System.Drawing.Point(20,120)
    $setupdzipbtn.Add_Click({SetZiboUpdzip})
    $main_form.controls.add($setupdzipbtn)

    $updziptxtbox           = New-Object system.Windows.Forms.TextBox
    $updziptxtbox.multiline = $false
    $updziptxtbox.size      = New-Object System.Drawing.Size(600,200)
    $updziptxtbox.location  = New-Object System.Drawing.Point(180,140)
    $updziptxtbox.Font      = 'Microsoft Sans Serif,12'
    $updziptxtbox.Enabled   = $false
    $updziptxtbox.Text      = $ZIBO_UPD_FILE
    $main_form.controls.add($updziptxtbox)

    ## X-Plane Path
    $setxplpathbtn               = New-Object Windows.Forms.Button
    $setxplpathbtn.text          = "Set X-Plane Path"
    $setxplpathbtn.width         = 150
    $setxplpathbtn.height        = 40
    $setxplpathbtn.location      = New-Object System.Drawing.Point(20,190)
    $setxplpathbtn.Add_Click({Setxplpath})
    $main_form.controls.add($setxplpathbtn)

    $xplpathtxtboxlbl = New-Object System.Windows.Forms.label
    $xplpathtxtboxlbl.location  = New-Object System.Drawing.Point(180,190)
    $xplpathtxtboxlbl.AutoSize  = $true
    $xplpathtxtboxlbl.Text      = "Current selected X-Plane Path "
    $main_form.controls.add($xplpathtxtboxlbl)

    $xplpathtxtbox           = New-Object system.Windows.Forms.TextBox
    $xplpathtxtbox.multiline = $false
    $xplpathtxtbox.size      = New-Object System.Drawing.Size(600,200)
    $xplpathtxtbox.location  = New-Object System.Drawing.Point(180,210)
    $xplpathtxtbox.Font      = 'Microsoft Sans Serif,12'
    $xplpathtxtbox.Enabled   = $false
    $xplpathtxtbox.Text      = $XPLANE_PATH
    $main_form.controls.add($xplpathtxtbox)

    ##XPL Version
    $xplversiontxtboxlbl = New-Object System.Windows.Forms.label
    $xplversiontxtboxlbl.location = New-Object System.Drawing.Point(180,245)
    $xplversiontxtboxlbl.AutoSize = $true
    $xplversiontxtboxlbl.Text     = "X-Plane version "
    $main_form.controls.add($xplversiontxtboxlbl)

    $xplversiontxtbox           = New-Object system.Windows.Forms.TextBox
    $xplversiontxtbox.Multiline = $false
    $xplversiontxtbox.size      = New-Object System.Drawing.Size(100,200)
    $xplversiontxtbox.location  = New-Object System.Drawing.Point(180,265)
    $xplversiontxtbox.Font      = 'Microsoft Sans Serif,12'
    $xplversiontxtbox.Enabled   = $false
    $xplversiontxtbox.Text      = $XP_VERSION
    $main_form.controls.add($xplversiontxtbox)

    $xplfullversiontxtboxlbl = New-Object System.Windows.Forms.label
    $xplfullversiontxtboxlbl.location = New-Object System.Drawing.Point(350,245)
    $xplfullversiontxtboxlbl.AutoSize = $true
    $xplfullversiontxtboxlbl.Text     = "X-Plane release "
    $main_form.controls.add($xplfullversiontxtboxlbl)

    $xplfullversiontxtbox           = New-Object system.Windows.Forms.TextBox
    $xplfullversiontxtbox.Multiline = $false
    $xplfullversiontxtbox.size      = New-Object System.Drawing.Size(100,200)
    $xplfullversiontxtbox.location  = New-Object System.Drawing.Point(350,265)
    $xplfullversiontxtbox.Font      = 'Microsoft Sans Serif,12'
    $xplfullversiontxtbox.Enabled   = $false
    $xplfullversiontxtbox.Text      = $XP_FULL_VERSION
    $main_form.controls.add($xplfullversiontxtbox)

    ##Zibo Path
    $zibopathtxtboxlbl = New-Object System.Windows.Forms.label
    $zibopathtxtboxlbl.location = New-Object System.Drawing.Point(180,300)
    $zibopathtxtboxlbl.AutoSize = $true
    $zibopathtxtboxlbl.Text     = "Found Zibo Path "
    $main_form.controls.add($zibopathtxtboxlbl)

    $zibopathtxtbox           = New-Object system.Windows.Forms.TextBox
    $zibopathtxtbox.multiline = $false
    $zibopathtxtbox.size      = New-Object System.Drawing.Size(600,200)
    $zibopathtxtbox.location  = New-Object System.Drawing.Point(180,320)
    $zibopathtxtbox.Font      = 'Microsoft Sans Serif,12'
    $zibopathtxtbox.Enabled   = $false
    $zibopathtxtbox.Text      = $ZIBO_PATH
    $main_form.controls.add($zibopathtxtbox)

    ##Zibo version
    $ziboversiontxtboxlbl = New-Object System.Windows.Forms.label
    $ziboversiontxtboxlbl.location  = New-Object System.Drawing.Point(180,355)
    $ziboversiontxtboxlbl.AutoSize  = $true
    $ziboversiontxtboxlbl.Text      = "Found Zibo version "
    $main_form.controls.add($ziboversiontxtboxlbl)

    $ziboversiontxtbox           = New-Object system.Windows.Forms.TextBox
    $ziboversiontxtbox.multiline = $false
    $ziboversiontxtbox.size      = New-Object System.Drawing.Size(250,300)
    $ziboversiontxtbox.location  = New-Object System.Drawing.Point(180,375)
    $ziboversiontxtbox.Font      = 'Microsoft Sans Serif,12'
    $ziboversiontxtbox.Enabled   = $false
    $ziboversiontxtbox.Text      = $ZIBO_VERSION
    $main_form.controls.add($ziboversiontxtbox)


    ## Draw a line....
    $line           = New-Object System.Windows.Forms.TextBox
    $line.Multiline = $false
    $line.Location  = New-Object System.Drawing.Point(10,450)
    $line.BackColor = "Black"
    $line.Enabled   = $false
    $line.Height    = 1
    $line.Width     = 870
    $line.AutoSize  = $false

    ## Option buttons

    ##Instal full
    $InstallFullbtn               = New-Object Windows.Forms.Button
    $InstallFullbtn.text          = "Install full Zibo"
    $InstallFullbtn.width         = 220
    $InstallFullbtn.height        = 60
    $InstallFullbtn.Font          = 'Microsoft Arial,14'
    $InstallFullbtn.location      = New-Object System.Drawing.Point(70,470)
    $InstallFullbtn.Add_Click({InstallFullZibo})
    $main_form.controls.add($InstallFullbtn)

    ##Instal Update
    $InstallUpdBtn               = New-Object Windows.Forms.Button
    $InstallUpdBtn.text          = "Update existing Zibo"
    $InstallUpdBtn.width         = 220
    $InstallUpdBtn.height        = 60
    $InstallUpdBtn.Font          = 'Microsoft Arial,14'
    $InstallUpdBtn.location      = New-Object System.Drawing.Point(330,470)
    $InstallUpdBtn.Add_Click({UpdateZibo})
    $main_form.controls.add($InstallUpdBtn)

    ##Instal full+Update
    $InstallAllBtn               = New-Object Windows.Forms.Button
    $InstallAllBtn.text          = "Install + update Zibo"
    $InstallAllBtn.width         = 220
    $InstallAllBtn.height        = 60
    $InstallAllBtn.Font          = 'Microsoft Arial,14'
    $InstallAllBtn.location      = New-Object System.Drawing.Point(590,470)
    $InstallAllBtn.Add_Click({InstallUpdateZibo})
    $main_form.controls.add($InstallAllBtn)



    $main_form.controls.add($line)

    $main_form.ShowDialog() | Out-Null
}

## Menu functions

function ShowAbout {
    Add-Type -AssemblyName PresentationFramework
    [void] [System.Windows.MessageBox]::Show( "EzZibo $EzZiboVERSION By Alex_NL74", "About EzZibo $EzZiboVERSION", "OK", "Information" )
}

function ShowPopup {
    param(
        [Parameter(Mandatory)]
        [string]$Notification,

        [ValidateSet("Information","Error")]
        [string]$Icon = "Information"
    )
    Add-Type -AssemblyName PresentationFramework
    [void] [System.Windows.MessageBox]::Show( $Notification, "Message", "OK", $Icon  )
}

function ShowManual {
    $Manual=@"
    **EzZibo $EzZiboVERSION Manual**

    It is simple.....
    Push the buttons to do stuff....
    This will work either for XP11 or XP12 installations...

    Install Zibo without updates:
    -Click the "Set Zibo full zip file"
     Browse to the full zipfile and click ok.
    -Click "Set X-Plane Path"
     Browse to your base xplane path and click ok.
    -Click "Install Full Zibo"
     Wait a few minutes and you are done.

    Install Zibo with updates:
    -Click the "Set Zibo full zip file"
     Browse to the full zipfile and click ok.
    -Click "Set Zibo update file"
     Browse to the update zipfile and click ok
    -Click "Set X-Plane Path"
     Browse to your base xplane path and click ok.
    -Click "Install + Update Zibo"
     Wait a few minutes and you are done.

    Update an existing Zibo installation
    -Click "Set Zibo update file"
     Browse to the update zipfile and click ok.
    -Click "Set X-Plane Path"
     Browse to your base xplane path and click ok.
    -Click "Update Zibo"
     Wait a few minutes and you are done.


    If you find a bug, or want a feature added, do let me know!


    Grtz,
    Alex_NL74
"@

Add-Type -AssemblyName PresentationFramework
[void] [System.Windows.MessageBox]::Show( "$Manual", "Message", "OK", "Information"  )
}

Function ShowReleaseNotes {
    Add-Type -AssemblyName PresentationFramework
    [void] [System.Windows.MessageBox]::Show( "$RELEASE_NOTES", "Message", "OK", "Information"  )
    }


## Menu button functions
function SetZiboFullzip($initialDirectory="C:\Users\$Env:UserName\Downloads") {

    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null
    $ZiboFullzip                    = New-Object System.Windows.Forms.OpenFileDialog
    $ZiboFullzip.Title              = "Select the Full Zibo zip File"
    $ZiboFullzip.Filter             = "Zip Files (*.zip)|*.zip"
    $ZiboFullzip.initialDirectory   = $initialDirectory

    if($ZiboFullzip.ShowDialog() -eq "OK")
    {   $global:ZIBO_FULL_FILE = $ZiboFullzip.FileName
        #First lets check if the filesize is what we expect. more then 1.6GB
        $filesize =  [math]::Round(((get-Item "$ZIBO_FULL_FILE").Length/1mb),0)

        If ($filesize -lt 1600) {
        ShowPopup -Notification "File is too small. This can not be a full release of the zibo plane" -Icon "Error"
        return
        }

        #Let test if the zip file name contains "full"
        if ( -not ($ZIBO_FULL_FILE -like "*full*")) {
            ShowPopup -Notification "Filename does not contain the word full. This can not be a full release of the zibo plane" -Icon "Error"
            return
        }
        #updating textbox
        $fullziptxtbox.Text      = $ZIBO_FULL_FILE
        $fullziptxtbox.Refresh()

    }
}


function SetZiboUpdzip($initialDirectory="C:\Users\$Env:UserName\Downloads")
    {
        [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null
        $ZiboUpdzip                     = New-Object System.Windows.Forms.OpenFileDialog
        $ZiboUpdzip.Title               = "Select the Zibo update File"
        $ZiboUpdzip.Filter              = "Zip Files (*.zip)|*.zip"
        $ZiboUpdzip.initialDirectory    = $initialDirectory

        if($ZiboUpdzip.ShowDialog() -eq "OK")
        {   $global:ZIBO_UPD_FILE = $ZiboUpdzip.FileName

            #First lets check if the filesize is what we expect. less then 50mb
            $filesize =  [math]::Round(((get-Item "$ZIBO_UPD_FILE").Length/1mb),0)

            If ($filesize -gt 100) {
            ShowPopup -Notification "File is too big. This can not be an update release of the zibo plane" -Icon "Error"
            return
            }
            $updziptxtbox.Text      = $ZIBO_UPD_FILE
            $updziptxtbox.Refresh()
        }

}

Function Setxplpath($initialDirectory=""){

        [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null

        $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
        $foldername.Description     = "Select a folder"
        $foldername.rootfolder      = "MyComputer"
        $foldername.SelectedPath    = $initialDirectory

        if($foldername.ShowDialog() -eq "OK") {
            $temppath=$foldername.SelectedPath
            if (Test-Path -Path "$temppath\X-Plane.exe" -PathType Leaf){
                $global:XPLANE_PATH     = $foldername.SelectedPath
                $xplpathtxtbox.Text     = $global:XPLANE_PATH
                $xplpathtxtbox.Refresh()

                ## Lets get the installed version on Xplane.
                $a = ((get-item $XPLANE_PATH\X-Plane.exe).VersionInfo.ProductVersion)
                $global:XP_FULL_VERSION = $a
                $global:XP_VERSION = "XP" + ($a.Substring(0, [Math]::Min($a.Length, 2)))
                $xplversiontxtbox.Text      = $global:XP_VERSION
                $xplfullversiontxtbox.Text      = $global:XP_FULL_VERSION
                $xplversiontxtbox.Refresh()
                $xplfullversiontxtbox.Refresh()

                ## If we found the X-Plane we will see if we can find a Zibo install
                ## First we check if we do not find more then 1 Zibo
                $ZiboSearchResult = (get-childItem "$global:XPLANE_PATH\Aircraft\" -Recurse -Depth 3 -filter b738_4k.acf)

                if (($ZiboSearchResult).Count -gt 0) {
                    $ZIBO_PATH_COUNT = (($ZiboSearchResult).DirectoryName).Count
                    if ($ZIBO_PATH_COUNT -gt 1){
                        ShowPopup -Notification "EzZibo found $ZIBO_PATH_COUNT Zibo Installations. `r`nEzZibo can not work with more then 1 Zibo install!" -Icon "Error"
                        $global:XPLANE_PATH    = "UNSET!"
                        $xplpathtxtbox.Text    = $XPLANE_PATH
                        $xplpathtxtbox.Refresh()
                        return
                    }
                }

                ## Now see if there is a  Zibo path and update the variables and gui
                if (((($ZiboSearchResult).DirectoryName).Count) -eq 0) {
                    $global:ZIBO_PATH       = "No Zibo Found"
                    $zibopathtxtbox.Text    = "No Zibo Found"
                    $zibopathtxtbox.Refresh()
                    $global:ZIBO_VERSION    = "No Zibo version was found"
                    $ziboversiontxtbox.text = $ZIBO_VERSION
                    $ziboversiontxtbox.refresh()
                    return
                }

                if (((($ZiboSearchResult).DirectoryName).Count) -eq 1) {
                    $global:ZIBO_PATH = (($ZiboSearchResult).DirectoryName)
                    ## Checking for valid zibo install using .acf file
                    if (((select-string -path "$ZIBO_PATH\b738_4k.acf" -pattern "p acf/_name").line).Trim("P acf/_name ") -ne "Boeing 737-800X (4k)") {
                        $global:ZIBO_PATH       = "No Zibo Found"
                        $zibopathtxtbox.Text    = "No Zibo Found"
                        $zibopathtxtbox.Refresh()
                        $global:ZIBO_VERSION    = "No Zibo version was found"
                        $ziboversiontxtbox.text = $ZIBO_VERSION
                        $ziboversiontxtbox.refresh()
                        ShowPopup -Notification "The .acf file in selected folder is not a Zibo version" -Icon "Error"
                        return
                    }
                    $zibopathtxtbox.Text      = $ZIBO_PATH
                    $zibopathtxtbox.Refresh()
                }

                ## If we found a zibo, get the version number.
                if ($ZIBO_PATH -ne "No Zibo Found") {
                   (Select-String -Path "$ZIBO_PATH\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua" -Pattern "version = `"v") -match "v[\d\.rc]+"; $matches[0]
                   $global:ZIBO_VERSION     = $matches.Values[0]
                   $ziboversiontxtbox.text  = $ZIBO_VERSION
                   $ziboversiontxtbox.refresh()
                }
            }
        }
        else {
            ShowPopup -Notification "This is not the X-Plane root folder! Select the one where X-Plane.exe is!" -Icon "Error"
        }
    }


### PREREQUISITES!
function XPlaneRunning {
    $xplanepid = (Get-Process X-plane -ErrorAction SilentlyContinue).id
    if ($xplanepid){
    ShowPopup -Notification "Xplane is still running! `r`nPlease close Xplane be for running EzZibo! `r`nEzZibo will now close." -Icon "Error"
    Exit
    }
}

### INSTALL LOGIC!
function InstallUpdateZibo {
    #install the base Zibo
    InstallFullZibo
    #updating to selected update file
    UpdateZibo
}

function UpdateZibo {
    #checking Paths
    if ($ZIBO_UPD_FILE -eq "UNSET!") {
       ShowPopup -Notification "Zibo update file has not been set." -Icon "Error"
       return
    }
    if ($XPLANE_PATH -eq "UNSET!") {
       ShowPopup -Notification "X-Plane path has not been set." -Icon "Error"
       return
    }
    if ($ZIBO_PATH -eq "UNSET!" -or $ZIBO_PATH -eq "No Zibo Found" ) {
        ShowPopup -Notification "No Zibo mod found. Update can not be done." -Icon "Error"
        return
    }
    if ( -not (Test-Path -Path "$ZIBO_PATH")) {
        ShowPopup -Notification "Zibo folder is not there" -Icon "Error"
        $global:ZIBO_PATH       = "No Zibo Found"
        $zibopathtxtbox.Text    = "$ZIBO_PATH"
        $zibopathtxtbox.Refresh()
        $global:ZIBO_VERSION   = "No Zibo version was found"
        $ziboversiontxtbox.text = $ZIBO_VERSION
        $ziboversiontxtbox.refresh()
        return
    }
    #checking xplane version
    if (-not (($ZIBO_UPD_FILE).Contains("$global:XP_VERSION"))) {
        ShowPopup -Notification "Zibo update zip file and X-Plane version do not match! `r`nZibo update can not be done. `r`nPlease download a $global:XP_VERSION version of the zibo" -Icon "Error"
        return
    }
    #Checking for exixtence of the temp folder and remove it
    if (Test-Path -Path "$XPLANE_PATH\Aircraft\zibo\ezzibotemp") {
        Remove-Item -Force -Recurse -Path "$XPLANE_PATH\Aircraft\ZIBO\ezzibotemp"
    }
    #Expanding update file into tmp
    Expand-Archive "$ZIBO_UPD_FILE" -DestinationPath "$XPLANE_PATH\Aircraft\zibo\ezzibotemp"
    #checking version.txt file
    #Getting the version of the installed zibbo
    $ZIBOVERSIONTXT = (Get-Content $ZIBO_PATH\version.txt) -match '[0-9]*.[0-9]*'
    $ZIBOVERSIONTXT = $matches[0]
    #GEtting the version of the extracted update file
    $UPDATEVERSIONTXT= (Get-Content $XPLANE_PATH\Aircraft\Zibo\ezzibotemp\version.txt) -match '[0-9]*.[0-9]*'
    $UPDATEVERSIONTXT = $matches[0]
    if ($ZIBOVERSIONTXT -ne $UPDATEVERSIONTXT) {
        ShowPopup -Notification "This update file is not for the installed Zibo version!" -icon "Error"
        Remove-Item -Force -Recurse -Path "$XPLANE_PATH\Aircraft\zibo\ezzibotemp"
        return
    }
    #checking B738.a_fms.lua file if installed version is older then update file
    if ((Get-ChildItem -File $ZIBO_PATH\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua).LastWriteTimeUtc -gt (Get-ChildItem -File $XPLANE_PATH\Aircraft\ZIBO\ezzibotemp\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua).LastWriteTimeUtc) {
        ShowPopup -Notification "Installed version is newer then update file! Update failed." -Icon "Error"
        Remove-Item -Force -Recurse -Path "$XPLANE_PATH\Aircraft\ZIBO\ezzibotemp"
        return
    }
    #checking B738.a_fms.lua file if installed version is NOT the same as installed version
    if ((Get-ChildItem -File $ZIBO_PATH\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua).LastWriteTimeUtc -eq (Get-ChildItem -File $XPLANE_PATH\Aircraft\ZIBO\ezzibotemp\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua).LastWriteTimeUtc) {
        ShowPopup -Notification "Installed version is the same as the update file! Update failed." -Icon "Error"
        Remove-Item -Force -Recurse -Path "$XPLANE_PATH\Aircraft\ZIBO\ezzibotemp"
        return
    }
    #Updating into Zibo folder
    Copy-Item "$XPLANE_PATH\Aircraft\ZIBO\ezzibotemp\*" -Destination "$ZIBO_PATH" -Force -Recurse
    #Cleaning up temp folder
    Remove-Item -Force -Recurse -Path "$XPLANE_PATH\Aircraft\ZIBO\ezzibotemp"
    #updating the zibo version.
    (Select-String -Path "$ZIBO_PATH\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua" -Pattern "version = `"v") -match "v[\d\.rc]+"; $matches[0]
    $global:ZIBO_VERSION    = $matches.Values[0]
    $ziboversiontxtbox.text = $ZIBO_VERSION
    $ziboversiontxtbox.refresh()
    #displaying a popup of succesfull update
    ShowPopup -Notification "Zibo succesfully updates to $ZIBO_VERSION" -Icon "Information"
}

function InstallFullZibo {
    if ($ZIBO_FULL_FILE -eq "UNSET!") {
        ShowPopup -Notification "Zibo full file has not been set." -Icon "Error"
        return
    }
    if ($XPLANE_PATH -eq "UNSET!") {
        ShowPopup -Notification "X-Plane path has not been set." -Icon "Error"
        return
    }
    if ( -not ($ZIBO_PATH -eq "UNSET!" -or $ZIBO_PATH -eq "No Zibo Found" ) ) {
        ShowPopup -Notification "Zibo mod was already found. Full installation can not be done." -Icon "Error"
        return
    }
    #checking xplane versions
    if (-not (($ZIBO_FULL_FILE).Contains("$global:XP_VERSION"))) {
        ShowPopup -Notification "Zibo full zip file and X-Plane version do not match! `r`nFull installation can not be done. `r`nPlease download a $global:XP_VERSION version of the zibo" -Icon "Error"
        return
    }

    #Making initial folder if not exists
    If ( -not (Test-Path -Path "$XPLANE_PATH\aircraft\Zibo")) {
        New-Item "$XPLANE_PATH\aircraft\Zibo" -ItemType Directory
    }

    #Expanding Zibo into folder
    Expand-Archive "$ZIBO_FULL_FILE" -DestinationPath "$XPLANE_PATH\aircraft\zibo"

    #updating the zibo path.
    $global:ZIBO_PATH       = (Get-ChildItem "$XPLANE_PATH\Aircraft\" -Recurse | Where-Object { $_.PSIsContainer -and $_.Name.Contains("B737-800X")}).FullName
    $zibopathtxtbox.Text    = $ZIBO_PATH
    $zibopathtxtbox.Refresh()
    #updating the zibo version.
    (Select-String -Path "$ZIBO_PATH\plugins\xlua\scripts\B738.a_fms\B738.a_fms.lua" -Pattern "version = `"v") -match "v[\d\.rc]+"; $matches[0]
    $global:ZIBO_VERSION    = $matches.Values[0]
    $ziboversiontxtbox.text = $ZIBO_VERSION
    $ziboversiontxtbox.refresh()
    #displaying a popup of succesfull Install
    ShowPopup -Notification "Zibo succesfully installed into $ZIBO_PATH" -Icon "Information"
}

XPlaneRunning
Menu
# EzZiboInstaller

## Legal stuff
\
**USE AT OWN RISK!!!! I CAN NOT BE HELD RESPONSIBLE!!**

## General info
EzZibo is a small tool i made just to make installing Zibo and updating it a lot easier.\
Download the full zip and the latest update(no need for all updates) and tell the tool where they are.\
\
Beta testers are welcome please drop me a msg on discord.\
If you find bugs, please let me know as well on discord.

## Release info
**v1.2.3**\
-Minor fix in full installation logic where the zibo seach function looked in the xplane root instead of \Aircraft\
\
**v1.2.2**\
-minor fix in the update section to make sure temp folder is not there\
\
**v1.2.1**\
-Minor fix in .acf file search.\
\
**v1.2.0**\
-Fixed new versioning in version.txt.\
-fixed new version check in .acf file.\
-Fixed rename of full zip extraction when name was the same as expected.\
\
**v1.1.1**\
-Fixed update file size for 4.00rc3.8\
\
**v1.1.0**\
-Changed code for zibo folder detection to not mess up with the default 737 or the upcomming LU737NG\
\
**v1.0.1**\
-Put a heavy search query  for finding the Zibo into a variable array so i do not have to execute it 6 times.\
\
**v1.0**\
-Formatting of in app release notes changed.\
-Added a trap to catch if more then 1 zibo is installed.\
-Changed the way a zibo folder is detected to prevent not finding all if there are multiple\
-Added checking for X-Plane mismatch in full and updates\
\
**v0.9d**\
-Fixed bug: update to same verion is possible.\
-Added release notes to help menu.\
-Creation of zibo folder in aircraft is now with a capital Z.\
-Added conformation to succesful install.\
-Added conformation to succesful update.\
-Reverted release info, newest version is now on top\
\
**v0.9c**\
-Included a check if xPlane is running.\
-Small rewrite of zibofolder detect code to enhance performance a lot.\
\
**v0.9b**\
-Many bugfixes and added checks.\
-This is a point of something that should work.\
\
**v0.9a**\
-Initial Script.


## Manual
It is simple.....\
Push the buttons to do stuff....\
This will work either for XP11 or XP12 installations...\
\
**Install Zibo without updates:**\
-Click the "Set Zibo full zip file"\
Browse to the full zipfile and click ok.\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Install Full Zibo"\
Wait a few minutes and you are done.\
\
**Install Zibo with updates:**\
-Click the "Set Zibo full zip file"\
Browse to the full zipfile and click ok.\
-Click "Set Zibo update file"\
Browse to the update zipfile and click ok\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Install + Update Zibo"\
Wait a few minutes and you are done.\
\
**Update an existing Zibo installation**\
-Click "Set Zibo update file"\
Browse to the update zipfile and click ok.\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Update Zibo"\
Wait a few minutes and you are done.\
\
If you find a bug, or want a feature added, do let me know!
\
Grtz,\
Alex_NL74
